angular.module('underscore', []).factory('_', function() {
	return window._; // assumes underscore has already been loaded on the page
});
angular.module('vida', ['underscore'])
	.controller('actividadesCtrl', ['$scope', '$timeout', '_', function($scope, $timeout, _){
		$scope.generals = {
			'uiInsert' : false,
			'model': {
				'nombre': undefined
			},
			currentTemplate: 'table.html'
		};
		$scope.actividades = [
			{
				nombre: 'Pasear a tilin',
				creacion: new Date()
			}
			];
		$scope.onAddActividad = function() {
			$scope.generals.uiInsert = true;
		};
		$scope.onCancelCreacion = function() {
			$scope.generals.uiInsert = false;
		};
		$scope.onGuardar = function() {
			if ( $scope.generals.model.nombre !== undefined && $scope.generals.model.nombre.length > 0 ) {
				$scope.actividades.push({
					nombre: $scope.generals.model.nombre,
					creacion: new Date()
				});
				$scope.generals.model.nombre = "";
			}
		};
		$scope.onLostFocus = function() {
			$scope.generals.uiInsert = false;
			$scope.generals.model.nombre = "";
		};
		$scope.onSelectRow = function(row) {
			row.active = row.active?!row.active:true;
			if ( _.where($scope.actividades, {'active': true}).length > 0 ) {
				$scope.generals.uiRemove = true
			} else {
				$scope.generals.uiRemove = false;
			}
			
			$scope.generals.uiInsert = false;
		};
		$scope.uiInsertOrRemove = function() {
			return $scope.generals.uiRemove || $scope.generals.uiInsert;
		};
		$scope.onRemoveActividad = function() {
			$scope.actividades = _.reject($scope.actividades, function(data) {
				return data.active;	
			});
			if ( _.where($scope.actividades, {'active': true}).length > 0 ) {
				$scope.generals.uiRemove = true
			} else {
				$scope.generals.uiRemove = false;
			}			
		};
		$scope.onInfo = function() {
			$scope.generals.currentTemplate = $scope.generals.currentTemplate == 'info.html'?'table.html':'info.html';
		};
	}]);